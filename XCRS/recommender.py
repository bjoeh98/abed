"""This file contains all the computation for the recommendation and explanaition.
"""
import pickle
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
import difflib
class Recommender:
    def __init__(self):
        '''load the dataframe, the binary matrix, an empty prefrence array 
        the dictionary to map values to the position in the bianry matrix
        all of them are computed in the jupiter notebook 
        '''
        self.types = ['genres','keywords','actors','directors','rating','time','decade']
        self.movie_mat = np.load('movieData/movie_mat.npy')
        self.emptyPrefrences = np.load('movieData/pref_arr.npy')[0].tolist()
        self.importance_arr = np.load('movieData/importance_arr.npy')
        with open('movieData/movie2position', 'rb') as f:
            self.movie2position_dict = pickle.load(f)
        with open('movieData/movie_info', 'rb') as f:
            self.movie_info = pickle.load(f)
        with open('movieData/genres_dict', 'rb') as f:
            self.genres_dict = pickle.load(f)
        with open('movieData/keywords_dict', 'rb') as f:
            self.keywords_dict = pickle.load(f)
        with open('movieData/actors_dict', 'rb') as f:
            self.actors_dict = pickle.load(f)
        with open('movieData/directors_dict', 'rb') as f:
            self.directors_dict = pickle.load(f)
        with open('movieData/rating_dict', 'rb') as f:
            self.rating_dict = pickle.load(f)
        with open('movieData/time_dict', 'rb') as f:
            self.time_dict = pickle.load(f)
        with open('movieData/decade_dict', 'rb') as f:
            self.decade_dict = pickle.load(f)
        with open('movieData/value2position', 'rb') as f:
            self.allValue2position = pickle.load(f)
        with open('movieData/position2value', 'rb') as f:
            self.position2value = pickle.load(f)
        with open('movieData/changepoint','rb') as f:
            self.changepoint = pickle.load(f)
        self.recommended = []

      
    def recommend(self,prefrences,recommended):
        '''
        This function computes the cosinus simmilarity between the user prefrence vector and evrey film vector
        Return: It returns the movie with the highest simmalarity that was not yet recommended 
        '''
        if type(prefrences) != list:
            raise Exception("The prefrences have to be in a list form")
        pref_arr = np.array([prefrences])
        if pref_arr.shape[0] != 1 or pref_arr.shape[1] != self.movie_mat.shape[1]:
            raise Exception("The prefrence list has the wrong shape it should have {} entries".format(self.movie_mat.shape[1]))
        '''if np.sum(pref_arr[0]) == 0:
            raise Exception("There are no prefrences yet")'''
        result = cosine_similarity(self.movie_mat,pref_arr).flatten()
        n = len(recommended) + 1 # makes shure that at least one recommendation was not recommended before
        ind = np.argpartition(result, -n)[-n:]
        recommendation = ind[np.argsort(result[ind])]
        # the recommendations are sorted from lowest to highest, therfor it iterates from up to down 
        for i in range(-1,-n-1,-1):
            if recommendation[i] not in recommended:
                return recommendation[i]
        return recommendation

    def compute_why_not(self,prefrences,recommended_movie,foil_movie):
        '''This function gets a the movie_id of a foil and computes what values need to be changed to make the movie vecto
        param:  prefrences: list of prefrences 
                recommended_movie: movie that was recommended
                foil_movie: the movie_id that should be recommended
        return: tupel of add, delete, same
                add: list of value_ids that needs to be added to change recommendation towards foil
                delete: list of value_ids that needs to be deltet from prefrences to change recommendation towards foil
                same: tupel of add and delete would advance both recommende and foil 
        '''
        add = []
        delete = []
        same = ([],[])
        for i in range(len(prefrences)):
            if foil_movie[i] != prefrences[i]: #diffren between prefrence and foil
                if foil_movie[i] == recommended_movie[i]: #is same as recommended
                    if foil_movie[i]:
                        same[0].append(i) # has to be added for both
                    else:
                        same[1].append(i) #has to be deleted for both
                else: #diffrent from recommended
                    if foil_movie[i]: #has to be added
                        add.append(i) 
                    else: #has to be deleted
                        delete.append(i)
        return (add,delete,same)

    def filter_why_not(self,explanaition,max_amount = 6,movie_expertise = 1.0):
        '''This function takes a full explanaition and sorts it it uses the following maxims:
        1. values that are set by the user and need to be deletet have the highest priority 
        2. Values that are uncommen have a higher priority
            exception: for directors and actors: 
            they must be as common as the most uncommen actor/director in user prefrences
            (represented by movie_expertise)
        3. The number of deletion explanaition is limited to halfe
        it then returns the first max_amount values
        params: explanaition: the full explanaition created by compute_why_not
                max_amount: (int) number of values to be returnd
                movie_expertise: (float) the user knowlledge about actors and directors
                                (the closer to 0 the higher) 
        return: a list of dictionarys with evrey entry being one explanaition value
                action: what needs to happen either add or delete
                value: value that needs to be added or deletet
                value_type: type of the value that explains 
                ranking: the ranking of the explanaition (lower is better)
        '''
        add, delete, same = explanaition
        delete_explain = []
        for i in delete:
            value,value_type,ranking = self.postion2value_type_importance(i)
            delete_explain.append({'action':'delete','value':value,'value_type':value_type,'ranking':ranking})
        delete_explain.sort(key=lambda y: y['ranking'])
        add_explain = []
        for i in add:
            value,value_type,ranking = self.postion2value_type_importance(i)
            # uncommen actors and directors are only known to people with high movie expertise
            if (value_type == 'actors' or 'directors'):
                #therfore uncommen actors and directors get testet if they could be known by the user 
                if ranking < movie_expertise: 
                    # and if not then they get a high ranking so that they are filtered out 
                    ranking += 100000
            add_explain.append({'action':'add','value':value,'value_type':value_type,'ranking':ranking})
        add_explain.sort(key = lambda y: y['ranking'])
        if len(delete_explain)> max_amount//2:
            delete_explain = delete_explain[:max_amount//2]
        explain = delete_explain + add_explain
        if len(explain)< max_amount: 
            return explain
        else:
            return explain[:max_amount]

    def explain_why_not(self,prefrences,recommended,foil_movie_name):
        """This functions creates a counterfactual explanaition by first computing a full explanaition and then filtering it based on Millers principiles. 
        params: prefrences: list of prefrences 
                recommended: list of movie that was recommended
                foil_movie_name: the movie_name that should be recommended
        return: status: OK: was successfull or error code 
                explain: a list of dictionarys with evrey entry being one explanaition value
                        action: what needs to happen either add or delete
                        value: value that needs to be added or deletet
                        value_type: type of the value that explains 
                        ranking: the ranking of the explanaition (lower is better)
        """
        status = None
        found, foil_movie_id = self.movie2position(foil_movie_name,prefrences)
        if not found:
            simular = foil_movie_id
            status = "Not found"
            return (status,simular)
        if type(recommended) != list or len(recommended) == 0:
            status = "Nothing recommended yet"
            return (status,None)
        if foil_movie_id in recommended:
            status = "Already recommended"
            return (status,None)
        foil_movie = self.movie_mat[[foil_movie_id],:][0]
        if len(foil_movie) != len(prefrences):
            status = "Wrong shape"
            return (status,None)
        recommended_movie_id = recommended[-1]
        recommended_movie = self.movie_mat[recommended_movie_id]
        full_explanaition = self.compute_why_not(prefrences,recommended_movie,foil_movie)
        context = self.explain_context(prefrences=prefrences,returnType = 'dict')
        movie_expertise = 1.0
        for i in context['actors']:
            importance = self.postion2value_type_importance(self.value2position(i)[0])[2]
            if importance < movie_expertise: movie_expertise = importance
        for i in context['directors']:
            importance = self.postion2value_type_importance(self.value2position(i)[0])[2]
            if importance < movie_expertise: movie_expertise = importance
        explanaition = self.filter_why_not(explanaition=full_explanaition,max_amount = 6,movie_expertise=movie_expertise)
        status = 'OK'
        return (status,explanaition)


    def compute_why(self,movie_id, prefrences):
        '''
        gets a movie_id and computes the factual reasoning
        (not used)
        '''
        movie = self.movie_mat[movie_id]
        because = []
        for i in range(len(movie)):
            if movie[i] == prefrences[i] == True:
                because.append(i)
        return because

    def explain_context(self,prefrences,returnType = 'list'):
        '''
        parms: prefrences: list of user prefrences
                return type: either dict for dictionary or list for list 
        return either   a list of all prefrences with a tuple of value value type and value importance
                or      a dictionary for every type with the coresponding values the user has set  
        '''
        pref_dict = {'genres':[],'keywords':[],'actors':[],'directors':[],'rating':[],'time':[],'decade':[]}
        pref_list = []
        isNone = True 
        for i in range(len(prefrences)):
            if prefrences[i]:
                isNone = False
                value, value_type, value_importance = self.postion2value_type_importance(i)
                pref_dict[value_type].append(value)
                pref_list.append((value,value_type,value_importance))
        if isNone:
            return None
        if returnType == 'dict':
            return pref_dict    
        else:
            return pref_list

    def value2position(self,value,value_type=None):
        ''' Finds the corresponding position of a value
        uses value_type as specification, but finds also wrongly classified values
        when the value is unknown, returns set of most simular values
        params: value the value for which the position is searched
                value_type: the type of the value (actors, keywords...)
        return a tuple of:  position: position of the value, is None when the value is unknown
                            value_type: type of the value 
                            simular: when value not found, set of most simular values
        '''
        position = None 
        simular = None
        value_search = value.lower().replace(' ','')
        if value_type == 'genres':
            try:    
                position = self.genres_dict[value_search]
            except:
                simular = self.find_most_simular(value,self.genres_dict)
        elif value_type == 'keywords':
            try: 
                position = self.keywords_dict[value_search]
            except:
                simular = self.find_most_simular(value,self.keywords_dict)
        elif value_type == 'actors':
            try: 
                position = self.actors_dict[value_search]
            except:
                simular = self.find_most_simular(value,self.actors_dict)
        elif value_type == 'directors':
            try: 
                position = self.directors_dict[value_search]
            except:
                simular = self.find_most_simular(value,self.directors_dict)
        elif value_type == 'rating':
            try: 
                position = self.rating_dict[value_search]
            except:
                None
        elif value_type == 'time':
            try: 
                position = self.time_dict[value_search]
            except:
                None
        elif value_type == 'decade':
            try: 
                position = self.decade_dict[value_search]
            except:
                None
        if position == None:
            try:
                position = self.allValue2position[value_search]
                for j in self.types:
                    current_change = self.changepoint[j]
                    if position in range(current_change[0],current_change[1]):
                        value_type = j
            except:
                None
        return (position, value_type, simular)
    
    def postion2value_type_importance(self,position):
        '''
        The function maps positions to the values, value types and value importance
        param: position (int)
        return: tuple value, value type, importance (how rare is the value)
        '''
        value = self.position2value[position]
        value_importance = self.importance_arr[position]
        for j in self.types:
            current_change = self.changepoint[j]
            if position in range(current_change[0],current_change[1]):
                value_type = j
                return(value,value_type,value_importance)
        return None 
    
    def movie2position(self,movie_name,prefrences):
        ''' Finds the corresponding position of a movie
        when multiple movies exsist returns the movieposition that is most simular to user prefrences
        if no movie is found returns movie names that are simular to the users input. 
        params: movie_name: name of the movie
                prefrences: user prefrences
        return  either a tuple of:  found: true and position
                or a tuple of:      found: false and simular           
        '''
        movie_search = movie_name.lower().replace(' ','')
        try:
            positions = self.movie2position_dict[movie_search]
            found = True
        except:
            found = False
            simular = self.find_most_simular(movie_name,self.movie2position_dict,isMovie=True)
            return (found,simular)
        if len(positions) > 0:
                position = self.find_most_simular_movie(positions,prefrences)
        else:
            position = positions[0]
        return (found,position)

    def find_most_simular_movie(self,movie_list,prefrences):
        '''if there are multiple movies for one title,
         the one withe the highest simularity should be used
         params: movie_name: name of the movie
                prefrences: user prefrences
        return  position of most simular movie    
        '''
        pref_arr = np.array([prefrences])
        most_simular = (None,0,0)
        for i in movie_list:
            simularity = cosine_similarity(self.movie_mat[i,:].reshape(1, -1),pref_arr)[0,0]
            if most_simular[1] <= simularity:
                most_simular = (i,simularity)
        return most_simular[0]
    
    def position2movie(self,movie_id):
        '''takes the position and returns the whole movie information 
         params: movie_id: position of the movie
                
        return  dictionary with movie informations
        '''
        try:
            movie_info = self.movie_info[movie_id]
        except:
            movie_info = None
        return movie_info

    def missing_prefrence(self,prefrences):
        ''' find categorise that where no user prefrences are stated,
        and returns randomly one of them
        param: user prefrences: list of user prefrences
        return: a missing category (actors, genres, keywords ...)
        '''
        missings =[]
        for j in self.types:
            current_change = self.changepoint[j]
            start = current_change[0]
            stop = current_change[1]
            if sum(prefrences[start:stop]) == 0:
                missings.append(j)
        random = np.random.randint(0,len(missings))
        missing = missings[random]
        return missing

    def find_most_simular(self,value,dictionary,isMovie = False):
        ''' gets a value and finds the most simular written value in the data set
        params: value: the value for which simulars sould be found 
                dictionary: the dictionary against which should be compared
                isMovie: is the value a Movie, if yes than true
        return: a list of maximal 4 simular written values. 
        '''
        simular = difflib.get_close_matches(word=value,possibilities= dictionary.keys(),n =4,cutoff=0.5)
        result = []
        for i in simular:
            if isMovie:
                pos = dictionary[i][0]
                result.append(self.position2movie(pos)['title'])
            else:
                pos = dictionary[i]
                result.append(self.position2value[pos])
        return result

def test():
    import time
    r = Recommender()
    pref = r.emptyPrefrences
    pref[325] = True
    pref[243] = True
    pref[1] = True
    pref[4378] = True
    pref[r.value2position('Hugh Jackman')[0]] = True
    recommendation = r.recommend(pref,[])
    recommended = [recommendation]  
    print(r.position2movie(recommendation))
    explain = r.explain_why_not(pref, recommended, 'Les Misérables')
    print(explain) 
    multipel_movies = r.movie2position('Les Miserables',pref)
    print('wrong written',multipel_movies)
    #print(r.missing_prefrence(pref))
    #print(r.position2movie(5))
    #print('plot' in r.movie_info[0].keys())
    start = time.time()
    #print(r.value2position("tam Hanks",'actors'))
    #print(time.time()-start)
    print(r.movie_mat.shape)
    movie_name = 'Les Misérables'
    movie_search = movie_name.lower().replace(' ','')
    print(r.movie2position_dict[movie_search])
#test()



