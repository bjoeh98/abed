# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions
""" The module creates first an instead of the recommender class. 
The custom action then extracts the entities of the last message, recives slots like prefrences and computes its task with the help of the recommender object.
The result from the recommender object gets than transformed to an output and the slots are updated.
"""

# This is a simple example for a custom action which utters "Hello World!"

# from typing import Any, Text, Dict, List
#
# from rasa_sdk import Action, Tracker
# from rasa_sdk.executor import CollectingDispatcher
#
#
# class ActionHelloWorld(Action):
#
#     def name(self) -> Text:
#         return "action_hello_world"
#
#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
#
#         dispatcher.utter_message(text="Hello World!")
#
#         return []
from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
import recommender
#from rasa_sdk.forms import FormAction

recommender = recommender.Recommender()


class ActionAdd(Action):
    def name(self) -> Text:
         return "action_add"
    def run(self, dispatcher: CollectingDispatcher,
             tracker: Tracker,
             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        pref_array = tracker.get_slot('prefrences')
        if pref_array == None:
            pref_array = recommender.emptyPrefrences
        entities = tracker.latest_message['entities']
        value = []
        hits = [] # values that are found
        no_hits = [] # values that are not found 
        for i in entities:
            value_type_i = i['entity']
            value_i = i['value']
            if value_type_i == 'minutes':
                try:
                    minutes = int([int(s) for s in value_i.split() if s.isdigit()][0])
                    if minutes <= 90: 
                        value_i = 'short'
                    # midelthird (midel)
                    elif minutes <= 103:
                        value_i = 'medium'
                    # first third (long)
                    else:
                        value_i = 'long'
                    value_type = 'time'
                except:
                    continue
            if value_type_i == 'decade':
                length = 0
                while not value_i[length].isdigit() and length < len(value_i):
                    length +=1
                try:
                    value_i = '19'+value_i[length:length+2]
                except:
                    continue
            if value_type_i == 'year':
                try:
                    value_i = str((int(value_i) //10) *10)
                    value_type = 'decade'
                except:
                    continue
            if value_i not in value and value_type_i in recommender.types: 
                value.append(value_i)
                position, value_type, simular = recommender.value2position(value_i,value_type_i)
                if position == None:
                    no_hits.append((value_i,value_type_i,simular))
                else:
                    pref_array[position] = True
                    hits.append(value_i)
        if sum(pref_array) >= 3: 
            enoughInformation = True
        else: 
            enoughInformation = False
        for i in no_hits:
            value_i,value_type_i,simular = i
            if simular != None:
                buttons = []
                for j in simular:
                    title = j
                    payload_dict = {str(value_type_i):str(j)}
                    payload = "/add_prefrence{}".format(payload_dict)
                    button = {"title":title,"payload":payload}
                    buttons.append(button)
                dispatcher.utter_message(text="I didn't understand {} did you mean:".format(value_i),buttons=buttons)
            else:  
                dispatcher.utter_message(text="I do not know {} can you try somthing else".format(value_i))
        #if len(hits)>0:
            #dispatcher.utter_message(text="I added {}".format(", ".join(hits)))
        return [SlotSet('enoughInformation',enoughInformation),SlotSet('prefrences',pref_array)]

class ActionDelete(Action):
    def name(self) -> Text:
         return "action_delete"
    def run(self, dispatcher: CollectingDispatcher,
             tracker: Tracker,
             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        pref_array = tracker.get_slot('prefrences')
        if pref_array == None:
            pref_array = recommender.emptyPrefrences
            #dispatcher.utter_message(text="I know nothing about your movie prefrences, why don't you tell me what kind of movies you like?")
            return [SlotSet('prefrences',pref_array)]
        entities = tracker.latest_message['entities']
        value = []
        hits = [] # values that are found
        no_hits = [] # values that are not found 
        for i in entities:
            value_type_i = i['entity']
            value_i = i['value']
            if value_i not in value and value_type_i in recommender.types: 
                value.append(value_i)
                position, value_type, simular = recommender.value2position(value_i,value_type_i)
                if position == None:
                    no_hits.append((value_i,value_type_i,simular))
                    dispatcher.utter_message(text="I did not find "+str(value_i)+" !")
                else:
                    pref_array[position] = False 
                    hits.append((value_i,value_type))
                    dispatcher.utter_message(text="You don't want "+str(value_i)+" corresponding to position "+str(position))
        if sum(pref_array) >= 3: 
            enoughInformation = True
        else: 
            enoughInformation = False
        return [SlotSet('prefrences',pref_array),SlotSet('enoughInformation',enoughInformation)]

class ActionRecommend(Action):
    def name(self) -> Text:
         return "action_recommend"
    def run(self, dispatcher: CollectingDispatcher,
             tracker: Tracker,
             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        pref_array = tracker.get_slot('prefrences')
        if pref_array == None:
            pref_array = recommender.emptyPrefrences
        if sum(pref_array) == 0:
            dispatcher.utter_message(text="I know nothing about your movie prefrences, why don't you tell me what kind of movies you like?")
            return [SlotSet('prefrences',pref_array)]
        recommended = tracker.get_slot('recommended')
        if recommended == None:
            recommended = []
        recommendation = int(recommender.recommend(prefrences = pref_array,recommended=recommended))
        recommended.append(recommendation)
        recommendation_info = recommender.position2movie(recommendation)
        recomendation_name = recommendation_info['title']
        recomendation_image = recommendation_info['cover_image']
        buttons = [{"title": "Another one, please","payload": '/ask_recommendation'}, \
                {"title": "I like it","payload": '/rate{{"rate":"positive"}}'}, \
                {"title": "I don\'t like it","payload": '/rate{{"rate":"negative"}}'}, \
                {"title": "Facts","payload": '/what_fact'}, \
                {"title": "Why not ...","payload": '/what_foil'}, \
                {"title": "My prefrences","payload": '/ask_context'}]
        dispatcher.utter_message(text="I recommend {}".format(recomendation_name), image= recomendation_image, buttons=buttons)
        return [SlotSet('recommended',recommended)]



class ActionAskPrefrences(Action):
    def name(self) -> Text:
         return "action_ask_prefrences"
    def run(self, dispatcher: CollectingDispatcher,
             tracker: Tracker,
             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        ask ={'genres':"What kind of genres do you like?",
        'keywords':"Can you tell me what subjects you find intresting?",
        'actors':"Do you have some favorite actors?",
        'directors':"Any director in mind?",
        'rating':"How good should the movie be ranked?",
        'time':"Any prefrence for the length of the movie?",
        'decade':"Do you have a decade you like?"}
        pref_array = tracker.get_slot('prefrences')
        if pref_array == None:
            pref_array = recommender.emptyPrefrences
        missing_prefrence=recommender.missing_prefrence(pref_array)
        dispatcher.utter_message(text=ask[missing_prefrence])
        return []


class ActionExplainContext(Action):
    def name(self) -> Text:
         return "action_explain_context"
    def run(self, dispatcher: CollectingDispatcher,
             tracker: Tracker,
             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        pref_array = tracker.get_slot('prefrences')
        if pref_array == None:
            dispatcher.utter_message(text="I know nothing about your movie prefrences, why don't you tell me what kind of movies you like?")
            return []
        context = recommender.explain_context(pref_array)
        if context == None:
            dispatcher.utter_message(text="I had a problem in the back end please inform the programmer")
            return []
        buttons = []
        for i in context:
            value, value_type, value_importance = i
            title = value + " (" + value_type + ")"
            payload_dict = {str(value_type):str(value)}
            payload = "/delete_prefrence{}".format(payload_dict)
            button = {"title":title,"payload":payload}
            buttons.append(button)
        dispatcher.utter_message(text= "I belive you like (you can correct me by clicking on the button)",buttons=buttons)
        return []

class ActionExplainCounterfactual(Action):
    def name(self) -> Text:
         return "action_explain_counterfactual"
    def run(self, dispatcher: CollectingDispatcher,
             tracker: Tracker,
             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        entities = tracker.latest_message['entities']
        foil_movie = None
        for i in entities:
            if i['entity'] == 'movies':
                foil_movie = i['value']
                break
        if foil_movie == None:
            dispatcher.utter_message(text='I didn\'t understand the movie')
            return None
        pref_array = tracker.get_slot('prefrences')
        if pref_array == None:
            dispatcher.utter_message(text="I know nothing about your movie prefrences, why don't you tell me what kind of movies you like?")
            return []
        recommended = tracker.get_slot('recommended')
        if recommended == None or recommended == []:
            dispatcher.utter_message(text="I have not yet recommended any movie")
            return []
        status,explanaition = recommender.explain_why_not(pref_array,recommended,foil_movie)
        if status == 'OK':
            buttons=[]
            for i in explanaition:
                action= i['action']
                value= i['value']
                value_type = i['value_type']
                title = action + ": " + value + " (" + value_type + ")"
                if action == 'delete':
                    payload_dict = {str(value_type):str(value)}
                    payload = "/delete_prefrence{}".format(payload_dict)
                else:
                    payload_dict = {str(value_type):str(value)}
                    payload = "/add_prefrence{}".format(payload_dict)
                button = {"title":title,"payload":payload}
                buttons.append(button)
            dispatcher.utter_message(text= "To get {} you nedd to do the folowing".format(foil_movie),buttons=buttons)
        elif status =="Not found":
            simular = explanaition
            value_type = "movies"
            buttons=[]
            for j in simular:
                title = j
                payload_dict = {str(value_type):str(j)}
                payload = "/ask_counterfactual{}".format(payload_dict)
                button = {"title":title,"payload":payload}
                buttons.append(button)
            dispatcher.utter_message(text="I didn't understand {} did you mean:".format(foil_movie),buttons=buttons)

        else: 
            dispatcher.utter_message(text="There was an internal error: {}".format(status))
        return []

class ActionExplainFact(Action):
    def name(self) -> Text:
         return "action_explain_fact"
    def run(self, dispatcher: CollectingDispatcher,
             tracker: Tracker,
             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        recommended = tracker.get_slot('recommended')
        if recommended == None or recommended == []:
            dispatcher.utter_message(text="I have not yet recommended any movie")
            return []
        movie_id = recommended[-1]
        info = recommender.position2movie(movie_id)
        movie_name = info['title']
        entities = tracker.latest_message['entities']
        values = []
        all_values = ["plot","genres","keywords","actors","directors","year","length","rating"]
        for i in entities:
            if i['entity'] == 'value_types':
                value = i['value']
                if value not in values and value in all_values:
                    values.append(value)
        if values == []:
            dispatcher.utter_message(template="utter_what_fact")
            return []
        if 'all' in values:
            values = all_values
        for i in values:
            if i == "plot":
                plot = info['plot']
                message = "The plot of {} goes as follows:\n{}".format(movie_name,plot)
            elif i == "genres":
                genres = info['genres']
                message = "{} is a {} movie".format(movie_name,", ".join(genres))
            elif i == "keywords":
                keywords = info['keywords']
                message = "{} is about {}".format(movie_name, ", ".join(keywords))   
            elif i == "actors":
                actors = info['actors']
                message = "{} are in {}".format(", ".join(actors),movie_name)   
            elif i == "directors":
                directors = info['directors']
                message = "{} is directed by {}".format(movie_name, ", ".join(directors))  
            elif i == "year":
                year = info['year']
                decade = info['decade']
                message = "{} is from the {}s, to be precice {}".format(movie_name, decade, year) 
            elif i == "length":
                duration = info['duration']
                timeCategory = info['timeCategory']
                message = "{} is with {} minutes a {} movie".format(movie_name, duration, timeCategory) 
            elif i == "rating":
                rating = info['imdb_rating']
                ratingCategory = info['ratingCategory']
                message = "With a imdb rating of {} out of 10 {} is considert as an {} movie".format(rating,movie_name, ratingCategory) 
            else:
                continue
            dispatcher.utter_message(text=message)     
        return []

class ActionRate(Action):
    def name(self) -> Text:
         return "action_rate"
    def run(self, dispatcher: CollectingDispatcher,
             tracker: Tracker,
             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        recommended = tracker.get_slot('recommended')
        if recommended == None or recommended == []:
            dispatcher.utter_message(text="I have not yet recommended any movie")
            return []
        movie_id = recommended[-1]
        rated = tracker.get_slot('rated')
        if rated == None:
            rated =[]
            entities = tracker.latest_message['entities']
            values = []
            all_values = ["plot","genres","keywords","actors","directors","year","length","rating"]
            for i in entities:
                if i['entity'] == 'rate':
                    value = i['value']
                    break
            if value == "positive":
                buttons = [{"title": "Yes, please another movie","payload": '/ask_recommendation'}, \
                        {"title": "No I'm good; good bye","payload": '/goodbye'}]
                dispatcher.utter_message(text="I am happy that you liked it. Do you want another movie?",buttons=buttons)
            if value == "negative":
                buttons = [{"title": "new recommendation","payload": '/ask_recommendation'}, \
                    {"title": "restart","payload": '/restart'}, \
                    {"title": "Why not ...","payload": '/what_foil'}, \
                    {"title": "My prefrences","payload": '/ask_context'}]
                dispatcher.utter_message(text="Do you want another movie or an explanaition?",buttons=buttons)
            rated.append((recommended,value))
            return [SlotSet('rated',rated)]
        return []
