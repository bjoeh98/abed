# ABED
ABED stands for Artificial Bot that can Explaine its Descisions. 
Abed is a explainabel conversational movie recommender that recommends movies with the help of a conten-based vector space model recommender. 
The dialog managment is done with rasa, an open-source framework for designing bots and personal assistance.
The bot can explaine its recommendations based on three explanaition types:
- Result explanaition: States facts about the recommended movie, like cast or genres
- Context explanaition: States the currently by the system believed user prefrences
- Counterfactual explanaitions: The user can give a movie that was not recommended as foil and gets an explanaition why it was not recommended and how the user could get the recommendation

Rasa runs with python, it is tested for python 3.8 
For running ABED the following librerys have to be installed:
- rasa (https://rasa.com/docs/rasa/installation/)
- numpy (through pip)
- pickle (through pip)
- Rasa X is an easy solution for testing and deploying ABED (https://rasa.com/docs/rasa-x/installation-and-setup/installation-guide)
- For creating a own data set (increasing or decresing the amount of movies and attributes for the recommender) with the data_preparation.ipynb file both jupiter nootebook and pandas has to be installed 

For running abed:
- go into the XCRS folder 
- start the action server with rasa run actions 
- start the bot either with:
    - rasa shell (running it in the shell)
    - rasa x (running it with rasa x in the browser)


